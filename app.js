var { Builder } = require('selenium-webdriver');
var firefox = require('selenium-webdriver/firefox');
var { exec } = require('child_process');

exec('php -S localhost:8000', (err, stdout, stderr) => {
  if (err) {
    return;
  }

  console.log(`stdout: ${stdout}`);
  console.log(`stderr: ${stderr}`);
});

(async function example() {
    var driver = new Builder().forBrowser('firefox').build();
    await driver.get('http://localhost:8000/convert.html');
})();

