<?php

echo header('Content-Type: text/html; charset=utf-8');
$contents = isset($_POST['data']) ? $_POST['data'] : null;
$debug = isset($_GET['debug']) ? 1 : 0;

if ($debug) {
    $contents = array(
        array(
            'pathner' => 'THA',
            'id' => '1152386708916046801',
//            'description_text' => "ราคา 300 บาท\t\t  \t\t\t\t\tNew Set เซต 2 ชิ้น เสื้อพิมพ์ลายเปลือกหอย สไตล์ D&G งานผ้าชีฟอง+ กางเกงทรงเอวสูง ขายาว คุ้มเกินราคาแพทเทิลเป๊ะ ผ้าซาร่าเอวสม็อคหลัง งานหรูดูแพง สาวๆไม่ควรพลาดสี : 2 สีขนาด >>เสื้อ รอบอก32-36\" ยาว 16\" กางเกง รอบเอว(ยืดสม้อคหลัง)25-32\" สะโพก34-38\" ยาว40\"",
//            'description_text' => "Dress เดรสแขนกุดผูกโบว์ไข้ว เนื้อผ้าซาร่า เดรสแขนกุด  แตงผูกเอวริบบิ้นและมีกระเป๋าด้านข้าง  เนื้อผ้าใส่สบาย ทรงสวยหรู เก๋ๆ งานสวยจริง ใส่ได้ทุกโอกาสคะ มี 1 สีขนาด   รอบอก 34-36\" รอบเอว(ไม่ยืด)28-32\" สะโพก38-40\" ยาว 32\"",
//            'description_text' => "Set เสื้อแขนกุด แต่งระบายลายดอก มาคู่กันกับกางเกงลายดอก เอวสม้อค แต่งคลุมมุ้ง ด้านข้างแต่งแถบขาว สวยและน่ารักไปอีกค่ะ งานพร้อมส่ง เหมาะกับอากาศบ้านเราสุดๆ รีบตำกันด่วนเลยค่าสี   2 สี ดำ ขาวขนาด   รอบอกฟรีไซส์32-40\" ยาว23\" กางเกง รอบเอวสม้อค26-34\" สะโพก36-40\" ยาว12\"",
//            'description_text' => "set 2 ชิ้น เสื้อยืดแขนยาว ผ้าคอตตอน + เอี๊ยมจั้มสูทลายริ้วขาทรงกระบอกเนื้อผ้าโฟร์เวย์ มีกระเป๋าล้วง 2 ข้างมีซิปหลัง  มาพร้อมสายผูกจะผูกเอว หรือใส้ผูกเป็น Choker ก็ได้ค่ะ ขนาด-เสื้อ รอบอก 34-36\" ยาว 18\" เอี๊ยมจั้มสูท รอบอก 34-36\"  รอบเอว 25-30\" สะโพก 36-40\" ยาว 50\"สี   2 สี",
//            'description_text' => "Dress เดรสลายไพ่ใหม่ล่าสุด เนื้อผ้าซาร่าพิมพ์ลายคมชัด ทรงสวยขาดีสุดๆ จ้า สาวๆรีบจองเลยขนาด- รอบอก 34-34\"  รอบเอว 26-34\" สะโพก 34-38\" ยาว 31\"สี   1 สี",
//            'description_text' => "Maxi Dress แม็กซี่เดรสคอวี งานพิมพ์ลายสไตล์ D And G สุดหรู ที่สาวๆต้องมี  คราวนี้มาในงานผ้าไมโครค่ะ พรีเมี่ยมสุดๆ สาวๆคนไหนช้าบอกเลยว่าพลาดค่ะ รีบจองกันด่วนๆๆๆๆ จ้า พร้อมส่งขนาด- รอบอก 34-34\"  รอบเอว 25-30\" สะโพก 36-42\" ยาว 46\"สี   2 สี ขาว ดำ",
//            'description_text' => "เสื้อแขนกุดผ้าลูกไม้ซีทรูทั้งตัวคอวี ใส่คู่กับกางเกงขาบานทรงห้าส่วน งานผ้าโฟเวย์เนื้อดี ตัดเย็บส่วนมาก แต่งกระดุมหน้า 1 เม็ด เอวยืดหลัง กางเกงจับจีบทวิสหน้า ใส่แมทซ์กันเรียบหรูดูดีสุดๆ งานสวยมากแนะนำเลยค่ะขนาดเสื้อ    รอบอก 32-35 ยาว 22 นิ้วขนาดกางเกง    รอบเอว 25-32 สะโพก 34-42 ยาว 28 นิ้ว",
//            'description_text' => "เซ็ต 2 ชิ้น เสื้อยืดคอกลมแขนสั้นอกพิมพ์ลาย Pancoatตาโต ผ้าคอตตอลยืดหนานิ่มใส่สวย มาพร้อมกางเกงขาห้าส่วนผ้าเดียวกัน พิมพ์ลายหน้าขา เอวยืดมีกระเป๋าล้วง ใส่แมทซ์กันน่ารักจ้าขนาดเสื้อ    รอบอก 32-36 ยาว 22 นิ้วขนาดกางเกง    รอบเอว 25-32 สะโพก 34-38 ยาว 36 นิ้ว",
//            'description_text' => "เดรสยาวคอเชิ้ต ผ่าอกหน้าติดกระดุม ใส่เป็นเสื้อคลุมเกร๋ๆก็ได้น๊าาา มีเชือกสำหรับผูกเอวเป็นโบว์ปรับขนาดได้ งานแขนยาว ผ้าหนังกบพิมพ์ลายชอกชบา สวยมากค่ะ ใส่แหวกหน้าเก๋ๆได้เลย งานขายดี จัดเข้าตู้ไว้หยิบใส่ได้บ่อยๆจ้าขนาด    รอบอก 34-38 รอบเอว 25-34 สะโพกฟรีไซด์ ยาว 44 นิ้ว",
//            'description_text' => "ยีนส์แท้ขาสั้นแต่งโค้ง งานยีนส์เนื้อดีใส่สบายแมทซ์กับอะไรก็สวย งานนี้มีเสื้อเกาะอกสายเดี่ยวสีดำพื้นแถมให้จ้า ใส่ชิลๆ เริ่ดๆได้เลยL รอบเอว 29-30 สะโพก 37-38 นิ้ว",
//            'description_text' => "\t\t\t\t\tSet  2ชิ้น เสื้อแขนสั้น ผ้าคอตตอล มพร้อมเอี๊ยมกางเกงขาสั้น งานผ้าฮานาโกะ สีดำ ใส่เข้าชุดกันน่ารักๆ  หรือจะจับ มิกแอนด์แมชกับชุดอื่นก็สวยเก๋ค่ะ พร้อมส่ง ขนาด-เสื้อ รอบอก 34-36\" ยาว 26\"  เอี๊ยม  รอบเอว 26-30\" สะโพก 36-40\" ยาว 29\"สี   5 สี",
//            'description_text' => "\t\t\t\t\tเดรสแขนสั้น เนื้อผ้าซาร่าทรงสวย โทนสีพื้นเรียบดูเรียบร้อย ชายกระโปรงแต่งระบายเฉียงดูมีมิติ สวยหรูในชุดเดียว มีซิปหลัง พร้อมส่งสี  4 สีขนาด -รอบอก32-34\" รอบเอว24-28\" สะโพก34-38\" ยาว 34\"",
//            'description_text' => "จั้มสูทขาสั้นผ้าหนังกบพิมพ์ลายดอกไม้ งานคอวีป้ายแขนสั้น ต่อผ้าป้ายเป็นเหมือนใส่กระโปรงด้านหน้าเฉียงแต่งระบาย งานซิปหลังเอวไม่สม้อค งานใส่สวยน่ารักน่ารัก ทรงสวยเก๋ งานสวยจริง สาวๆห้ามพลาดเลยคะ สินค้าพร้อมส่งจ้ามี  5 สีขนาด    รอบอก 34-36'' รอบเอว(ไม่ยืด) 25-30'' สะโพก 34-38 \"จากไหล่ถึงเป้า 30 \" ยาว 31 \"",
//            'description_text' => "Set 2 ชิ้น เสื้อลายกราฟฟิก ปลายแขนแต่งระบาย ผ้าซีฟองใส่สบาย มาคู่กันกับกางเกงขายาวห้าส่วน ผ้าซาร่าเนื้อดี มีกระเป๋าล้วง 2 ข้าง ขาทรงกระบอกกว้างๆ มีซิปข้างสี   1 สีขนาด   เสื้อ รอบอก 32-38\"  ยาว22\" \/  กางเกง รอบเอว 29-30\"  สะโพก 34-40\" ยาว 33\"",
//            'description_text' => "งานป้ายBota จั้มสูทขาสั้นพิมลายดอกทานตะวัน  จั้มสูทขาสั้นพิมพ์ลายดอกทานตัว  ผ้าซีฟองมีชับ แขนแต่งระบาย ผ้าตาข่ายลายปัก คอวีด้านหลังและมีชิบด้านหลัง  ทรงสวยเก๋ งานสวยจริง พร้อมส่งจร้ามี 2 สี กรมท่า\/ส้มขนาด   M  รอบอก 32-34\"รอบเอว(ไม่ยืด)27-28\" สะโพก34-38\" จากไห่ลถึงเป้า29 ยาว 30\"",
//            'description_text' => "\t\t\t\t\tSet 2 ชิ้น เสื้อยืดคอกลม แขนยาว + เอี๊ยมกางเกงขายาว เนื้อผ้าโฟร์เวยื สายเดี่ยวแบบผูกมัน สามารถปรับระดับสายได้ เย็บแต่งกระเป๋าตรงหน้าอก มีกระเป๋าล้วง 2 ข้าง มีซิปหลัง ทรงขากางเกงเป็นทรงขากระบอก ใส่แล้วดูเพรียว ดูสูงค่ะ จะปล่อยขาหรือพับขึ้นก็เก๋ พร้อมส่ง สี   2 สี ดำ น้ำตาลขนาด   เสื้อ รอบอก34-38” ยาว 18\" เอี๊ยม รอบเอว25-30” สะโพก36-40” ยาว48”",
//            'description_text' => "\t\t\t\t\tจั้มสูทไหล่ล้ำขายาว ดีเทลช่วงคอเป็นยางยืดแต่งระบายน่ารักๆ เนื้อผ้าโรนัลโด้ พิมพ์ลายดอกสวยสด งานมีซิปหลัง กางเกงมีซับใน งานสวยมาก ใส่แล้วพริ้วสูงยาวสวยจ้าสี   4 สี ขาว ดำ ชมพู ฟ้าขนาด   รอบอก34-36” รอบเอว25-30” สะโพก36-42” ยาว52”",
//            'description_text' => "Maxi Dress ยีนส์เทียม ใส่สบาย ไม่ร้อน เหมาะกับอากาศบ้านเราสุดๆ สายตรงไหล่ผูกโบว์ ช่วงอกแต่งกระดุม มีซิปข้าง ปลายกระโปรงแต่งระบาย น่ารักสุดๆ สาวๆห้ามพลาดละน้าาาสี   อ่อน กลาง เข้มขนาด   รอบอก32-34\" รอบเอว25-30\" สะโพก42-46\" ยาว47\" (ปรับความยาวสายผูกไหล่ได้)",
//            'description_text' => "เสื้อกึ่งมินิเดรสทรงปาดไหล่ มีสายเดี่ยวเล็กๆค่ะ งานสีสดใสใส่แมทซ์กับกางเกงตัวโปรดกันได้เลย ดีเทลแต่งลูกไม้เนื้อหนานิ่มอย่างดีใส่ไม่คัน งานน่ารักมากห้ามพลาดขนาด    รอบอก 34-38 ยาว 27 นิ้ว",
//            'description_text' => "จั้มสูทขาสั้น ผ้าโฟเวย์ งานน่ารักใส่ได้บ่อยๆ ฮิตมว๊าก แนะนำเลย งานพิมพ์ลายดอกไม้ด้านหน้าลายคมชัน ด้านหลังเป็นสีพื้น มีซิปหลังยาวเอวไม่สม้อค งานสวยมาก จัดเลยจ้าขนาด     รอบอก 32-36 รอบเอว 25-30 สะโพก32-36 ยาว 29.5นิ้ว",
//            'description_text' => "\t\t\t\t\tSet 2 ชิ้น เอี๊ยมจั้มสูทขายาว สายปรับได้ เนื้อผ้าโฟร์เวย์ แต่ง��ทปมีซิปหลัง + เสื้อสายเดี่ยวเข้าชุด พร้อมส่งสี  2 สีขนาด -เสื้อ รอบอก32-34\" ยาว 19\" เอี๊ยมจั้มสูท รอบอก 34-36\" รอบเอวฟรีไซส์  สะโพก36-40\" ยาว53\"(ปรับได้)",
//            'description_text' => "งานป้ายBota จั้มสูทขาสั้นปักลายดอก  จั้นสูทขาสั้นปักลายดอกผ้าไหมแก้ว มีชับด้านในและชิบด้านหลัง ทรงสวยหรู สไตร์เก๋ น่าารักน่ารัก ใส่ได้ทุกโอกาสคะมี 2 สีขนาด   M  รอบอก 32-34\"รอบเอว(ไม่ยืด)24-27\" สะโพก34-38\" จากไห่ลถึงเป้า28 ยาว 30\"L  รอบอก 34-36\"รอบเอว(ไม่ยืด)25-30\" สะโพก36-40\" จากไห่ลถึงเป้า 28\" ยาว 30\"",
//            'description_text' => "\t\t\t\t\tJumpsuit แขนกุดทรงคล้องคอ กางเกงขายาว ดีเทลเปรี้ยวปังค่ะ งานผ้าไหมอิตาลี่ลายริ้ว ขาวดำคัตติ้งดีงาม มีซิปหลัง  ใส่แล้วดูสวยแพง ดูมีออร่า ดูมีสเน่ห์ค่ะสี  1 สีขนาด - รอบอก32-34\" รอบเอว25-30\" สะโพก36-40\" ยาว 55\"",
//            'description_text' => "\t\t\t\t\tเดรสคอตั้งแขนกุดแต่งระบาย จับจีบอกวีตัดต่อผ้าซีทูสีดำ ตัวเดรสเป็นผ้าแก้วปักลายดอกไม้ทั้งชุด  โทนสีฟ้า มีซับในอย่างดี  แพทเทริน์เป๊ะ ค่ะ น่ารักฝุดๆไปเลยค้ารุ่นนี้มิกซ์กับรองเท้าส้นสูงเครื่องประดับเก๋ๆก็ดูน่ารักแล้วค่ะสี  1 สีขนาด -SIZE M รอบอก32-34\" รอบเอว24-27\" สะโพก34-38\" ยาว 47\"SIZE L รอบอก34-36\" รอบเอว24-29\" สะโพก36-40\" ยาว 47\"",
//            'description_text' => "\t\t\t\t\tเดรสแขนสั้น เนื้อผ้าซาร่าทรงสวย โทนสีพื้นเรียบดูเรียบร้อย ชายกระโปรงแต่งระบายเฉียงดูมีมิติ สวยหรูในชุดเดียว มีซิปหลัง พร้อมส่งสี  4 สีขนาด -รอบอก32-34\" รอบเอว24-28\" สะโพก34-38\" ยาว 34\"",
//            'description_text' => "Set 2 ชิ้น เสื้อลายกราฟฟิก ปลายแขนแต่งระบาย ผ้าซีฟองใส่สบาย มาคู่กันกับกางเกงขายาว���้าส่วน ผ้าซาร่าเนื้อดี มีกระเป๋าล้วง 2 ข้าง ขาทรงกระบอกกว้างๆ มีซิปข้างสี   1 สีขนาด   เสื้อ รอบอก 32-38\"  ยาว22\" \/  กางเกง รอบเอว 29-30\"  สะโพก 34-40\" ยาว 33\"",
//            'description_text' => "\t\t\t\t\tJumpsuit จั้มสูทขาสั้น แขนยาวดีเทลแต่งระบายอก เว้าไหล่เดี่ยว ช่วงบนพิมพ์ลายกราฟฟิคสไตล์ D And G กางเกงสีพื้น ทั้งชุดเป็นผ้าไมโคร มีกระเป๋าล้วง 2 ข้าง มีซิปหลังสี   2สีขนาด -รอบอก32-36\"รอบเอว25-30\" สะโพก34-38\" ยาว28\"",
            'description_text' => "Maxi Dress เดรสยาวทรงตรง ตรงหน้าอกสกรีนลายโลโก้ kenzo paris เนื้อผ้าไหมพรมร่อง แขนกุด ด้านหน้าช่วงกระโปรงผ่ายาว ทรงสวย ใส่สบายๆ ออกแนวแซ็กซี่นิดๆใส่ได้ทุกโอกาสคะสี  3 สี ดำ เทา โอรสขนาด--รอบอก 32-34\"รอบเอว\"ฟรีไซร์\" สะโพก32-36\" ยาว46\"",
//            'description' => "ราคา 300 บาท\r\n\t\t  \t\t\t\t\t\r\nNew Set เซต 2 ชิ้น เสื้อพิมพ์ลายเปลือกหอย สไตล์ D&amp;G งานผ้าชีฟอง+ กางเกงทรงเอวสูง ขายาว คุ้มเกินราคาแพทเทิลเป๊ะ ผ้าซาร่าเอวสม็อคหลัง งานหรูดูแพง สาวๆไม่ควรพลาด\r\n\r\nสี : 2 สี\r\n\r\nขนาด &gt;&gt;เสื้อ รอบอก32-36\" ยาว 16\" กางเกง รอบเอว(ยืดสม้อคหลัง)25-32\" สะโพก34-38\" ยาว40\"",
           'description' => "Set 2 ชิ้น เสื้อคลุม+เกาะอกลายดอก เสื้อคลุมแขนยาวเนื้อผ้าโลนัลโด้ พิมพ์ลายดอกคมชัด มีจั้มเกาะอกตัวในเอวยืดสม็อค ลายเดียวกัน สไตร์สวยเก๋ ออกแนวสาวเซ็กซี่ๆ น่ารักน่ารัก เนื้อผ้าใส่สบาย งานสวยจริง พร้อมส่งจ้าา\r\n\r\n****นางแบบใส่งานจริงจ้าา*****\r\n\r\nสี :4 สี กรมท่า,โอรส, ขาว, ฟ้า\r\n\r\nขนาด >>เสื้อคลุม รอบอกฟรีไซร์ 38\"ยาว 31 \"\r\nจั้มเกาะอก>>อก 32-34 \" รอบเอวยืดสม็อค 26-32\"สะโพก 36-40\" ยาว23 \"",
            // 'description' => "Mini Dress ลายดอกไม้ ลายสุดฮิตตลอดกาลมาแล้วจ้าาา คราวนี้มาเอาใจสาวๆเดรสสั้นกันบ้าง ตัวเดรสงานผ้าหางกระรอก เนื้อดีค่ะ ใส่สบาย เอวสม็อค ช่วงแขนทรงบอลลูน เว้าไหล่ คราวนี้เก๋กว่างเดิมจ้า ไม่ใช่สายผูกไหล่น๊าา แต่เย็บติดเชือกผูกโบว์ไว้ตรงไหล่เลย ไม่มีซับในค่ะ แต่ไม่โบ๊รับประกันเลยจ้า งานนี้ต้องบอกว่า ใส่แล้วเกิด ใส่แล้วปังแน่นอน คอนเฟิร์มเลยจ้าาา\r\n\r\nสี : 5 สี กรม ขาว เหลือง ฟ้า โอรน\r\n\r\nรอบอก 34-36\" รอบเอว 28-34\"สะโพก 36-40\" ยาว 35\"",
            // 'description' => "Maxidress เดรสผ้าโฟเวย์เนื้อดี ผ้าหนานิ่มสวยใส่สบายค่ะ มีน้ำหนักพริ้วสวย ดีไซด์ปกเชิ้ตผ่าหน้าตลอดแนว แขนยาวปลายแขนติดกระดุม ใส่แหวกหน้า ชาวมุสลิมใส่ได้ค่ะงานเรียบร้อย ห้ามพลาด\r\n\r\nขนาด :: รอบอก 32-36 รอบเอว 25-30 สะโพก 34-40 ยาว 51 นิ้ว\r\n",
            'dd' => "\r\nราคา  230 บาท\r\n\t\t  \t\t\t\t\t\r\nSet 2ชิ้น เสื้อผูกโบว์ไหล่ ทรงสวย น่ารัก เนื้อผ้าโฟร์เวย์ ใส่ดูดีมากๆค่า พร้อมกับกางเกงขาสั้น เอวยืด เนื้อผ้าโฟร์เวย์\r\n\r\nสี : 3สี\r\nขนาด :เสื้อรอบอก 32-36\" ยาว23\"\r\nกางเกง รอบเอวยืด 25-30\" สะโพก 36-38\" ยาว 14\"\r\n\r\n",
            'cost' => "230",
            'cost_bath' => "230 บาท",
            'pictures' => array(
                "https://obs.line-scdn.net/hzwOkJWGLOVlHfipXECg-IA4MLm5ZJmNaTCA-bFITKTwIcWVeGnViPgVGLjxbK2tcSH0/m800x1200",
                "https://obs.line-scdn.net/hfuIuq1SjJVtBYDZVFjYiIggSY2wJNSteSTkiaVgEYGwJPixeGzh0blhcbj9ZP3wPH28/m800x1200",
                "https://obs.line-scdn.net/heCw55dPgJm1_IDVjKHYhFDZSNFthf3RpIi4hA2cfY1pkeXw9Iyx3DG5OY1wzLClodSs/m800x1200",
                "https://obs.line-scdn.net/hAizKPinOAkdyXhFJJQgFPjssR35tBgoWL1FTd2syRHBjCgtFKQFYJmFgRnJpCw0WKwE/m800x1200",
                "https://obs.line-scdn.net/hm8l8VhudLhZkLT0YM3spby1fPi99IScUPy99dCFEbXd9JiZDbiV1c3BFbCF8cCEVbC8/m800x1200",
            )
        )
    );
}

static $pather = array();
$directory = dirname(__FILE__).DIRECTORY_SEPARATOR;

if (!empty($contents)) {
    foreach ($contents as $id => &$content)
    {
        if (empty($content['id']) || empty($content['description'])) {
            unset($contents[$id]);
            continue;
        }

        if (skipPreorder($content)) {
            unset($contents[$id]);
            continue;
        }

        $folder = $content['pathner'];
        $dirname = $directory.$folder.DIRECTORY_SEPARATOR;
        $content_dir = $dirname.$content['id'].DIRECTORY_SEPARATOR;
        $image_dir = $content_dir.'pictures'.DIRECTORY_SEPARATOR;

        $content['description'] = htmlspecialchars_decode($content['description']);
        $content['description_bak'] = $content['description'];

        topicEscape($content['description_text']);
        topicLimit($content['description_text']);
        clearDescriptionTag($content['description']);

        if (checkValidPrice($id, $contents)) {
            unset($contents[$id]);
            continue;
        }

        descriptionEscape($content['description']);
        getAttributes($content, $dirname);
        hashKey($content, $directory);

        if (!isset($pather[$content['pathner']])) {
            $pather[$content['pathner']] = true;

            createFolder($dirname);
        }

        createFolder($image_dir);
        if (!empty($content['pictures'])) {
            downloadImages($image_dir, $content['pictures']);
        }

        clearDetail($content);
        createJsonFile($content_dir, $content);
    }

    if (!empty($dirname) && file_exists($dirname.'data.json')) {
        $lists = file_get_contents($dirname.'data.json');

        if (!empty($lists)) {
            $contents = array_merge(json_decode($lists, true), $contents);
        }
    }

    createJsonFile($dirname, $contents);

    echo 'true';
    exit();
}

echo 'false';
exit();
/**
 * 
 * @param type $topic
 */
function topicEscape(&$content = null)
{
    $content = str_ireplace("\t", '', $content);
    $content = preg_replace("/(ราคา?|sell?|sale?).*?(\..*?บาท|\.บาท|บาท|\.|bath|฿|thb)(\s)*/i", '', $content);
    $content = preg_replace("/^(\xc2\xa0)*/i", '', $content);
    $content = str_replace("&", ' And ', $content);
    $content = str_replace("*", '+', $content);
    $content = str_replace("#", '', $content);
    $content = str_replace(":", ',', $content);
    $content = str_replace(">", '-', $content);
    $content = str_replace("..", ' ', $content);
    $content = str_replace("--", '-', $content);
    $content = str_replace(",", ' ', $content);
    $content = str_replace("(.)", ' ', $content);
    $content = str_replace(" ", " ", $content);
    $content = str_replace(chr(194).chr(160), chr(32), $content);
}


/**
 * 
 * @param type $content
 */
function descriptionEscape(&$content = null)
{
    $content = str_ireplace("\t", '', $content);
    $content = trim(preg_replace("/(ราคา?|sell?|sale?).*?(\..*?บาท|\.บาท|บาท|\.|bath|฿|thb)(\s)*/i", '', $content));
    $content = preg_replace("/^ */i", '', $content);
    $content = preg_replace("/^(<br>|<br\/>|<br \/>)*/i", '', $content);
    $content = preg_replace("/^(\xc2\xa0)*/i", '', $content);
    $content = preg_replace("/^ */i", '', $content);
    $content = preg_replace("/^(<br>|<br\/>|<br \/>)*/i", '', $content);
    $content = preg_replace("/^ */i", '', $content);
}


/**
 * 
 * @param type $content
 */
function topicLimit(&$content = null)
{
    $needles = array(
        'งานพร้อมส่ง' => strlen('งานพร้อมส่ง'),
        'พร้อมส่ง' => strlen('พร้อมส่ง'),
        'ไม่ควรพลาด' => strlen('ไม่ควรพลาด'),
        'ห้ามพลาดเลยค่ะ' => strlen('ห้ามพลาดเลยค่ะ'),
        'ห้ามพลาดเลยคะ' => strlen('ห้ามพลาดเลยคะ'),
        'ห้ามพลาดคะ' => strlen('ห้ามพลาดคะ'),
        'ห้ามพลาด' => strlen('ห้ามพลาด'),
        'ใส่ได้ทุกโอกาส' => strlen('ใส่ได้ทุกโอกาส'),
        'ทุกโอกาส' => strlen('ทุกโอกาส'),
        'ทุกฤดู' => strlen('ทุกฤดู'),
        'ทุกสถานะการณ์' => strlen('ทุกสถานะการณ์'),
        'ทุกที่' => strlen('ทุกที่'),
        'ด่วนเลย' => strlen('ด่วนเลย'),
        'แนะนำเลยค่ะ' => strlen('แนะนำเลยค่ะ'),
        'แนะนำเลย' => strlen('แนะนำเลย'),
        'ได้เลย' => strlen('ได้เลย'),
        'งานสวยจริง' => strlen('งานสวยจริง'),
        'สวยจริง' => strlen('สวยจริง'),
        'งานสวยมาก' => strlen('งานสวยมาก'),
        'สวยมาก' => strlen('สวยมาก'),
        'ทรงสวยเก๋' => strlen('ทรงสวยเก๋'),
        'สวยเก๋' => strlen('สวยเก๋'),
        'ทรงสวยหรู' => strlen('ทรงสวยหรู'),
        'สวยหรู' => strlen('สวยหรู'),
        'น่ารักจ๊ะ' => strlen('น่ารักจ๊ะ'),
        'น่ารักจ้า' => strlen('น่ารักจ้า'),
        'น่ารักจร้า' => strlen('น่ารักจร้า'),
        'น่ารักค่ะ' => strlen('น่ารักค่ะ'),
        'น่ารัก' => strlen('น่ารัก'),
        'สี ' => 0,
        'สี,' => 0,
        'สี-' => 0,
        'สีขนาด' => 0,
        'ขนาด-' => 0,
        'ขนาด  ' => 0,
        'ขนาด   ' => 0,
        'ขนาดเสื้อ' => 0,
        'รอบอก' => 0,
        'รอบเอว' => 0,
        'สะโพก' => 0,
    );

    foreach ($needles as $need => $combine)
    {
        if ($position = stripos($content, $need)) {
            $content = substr($content, 0, $position + $combine);
            break;
        }
    }
}


function getAttributes(&$content, $dirname)
{
    getCategories($content, $dirname);
    getColors($content, $dirname);
    getTextures($content, $dirname);
    getPatterns($content, $dirname);
    getLengths($content, $dirname);
}


function getCategories(&$content, $dirname)
{
    $needles = array(
        'set' => 'ชุดเข้าเซท',
        'Set' => 'ชุดเข้าเซท',
        'เซท' => 'เซท',
        'เสื้อยืด' => 'เสื้อยืด',
        'เสื้อปาดไหล่' => 'เสื้อปาดไหล่',
        'เสื้อครอป' => 'เสื้อครอป ท็อป',
        'เสื้อทรงใหญ่' => 'เสื้อทรงใหญ่',
        'เสื้อกล้าม' => 'เสื้อกล้าม/สายเดี่ยว',
        'สายเดี่ยว' => 'เสื้อกล้าม/สายเดี่ยว',
        'เกาะอก' => 'เกาะอก',
        'เสื้อเชิ้ต' => 'เสื้อเชิ้ต',
        'เสื้อโปโล' => 'เสื้อโปโล',
        'เสื้อคอเต่า' => 'เสื้อคอเต่า',
        'เสื้อคลุม' => 'เสื้อคลุม คาร์ดิแกน',
        'เสื้อสูท' => 'เสื้อสูท',
        'เสื้อฮู้ด' => 'เสื้อฮู้ด',
        'เสื้อโค้ท' => 'เสื้อโค้ท',
        'เสื้อกั๊ก' => 'เสื้อกั๊ก',
        'เสื้อแจ๊คเก็ต' => 'เสื้อแจ็คเก็ต',
        'เสื้อแจ็คเก็ต' => 'เสื้อแจ็คเก็ต',
        'Dress' => 'เดรส',
        'dress' => 'เดรส',
        'เดรส' => 'เดรส',
        'กระโปรงสั้น' => 'กระโปรงสั้น',
        'กระโปรงยาว' => 'กระโปรงยาว',
        'กระโปรงยีนส์' => 'กระโปรงยีนส์',
        'กระโปรงกางเกง' => 'กระโปรงกางเกง',
        'กระโปรง' => 'กระโปรงสั้น',
        'กางเกงขาสั้น' => 'กางเกงขาสั้น',
        'กางเกงขายาว' => 'กางเกงขายาว',
        'เลกกิ้ง' => 'เลกกิ้ง',
        'กางเกงยีนส์' => 'กางเกงยีนส์',
        'จั๊มสูท' => 'จั๊มสูท',
        'Jumpsuit' => 'จั๊มสูท',
        'jumpsuit' => 'จั๊มสูท',
        'เอี๊ยม' => 'เอี๊ยม',
        'เสื้อชั้นใน' => 'เสื้อชั้นใน',
        'กางเกงชั้นใน' => 'กางเกงชั้นใน',
        'ชุดนอน' => 'ชุดนอน',
        'ชุดกระชับสัดส่วน' => 'ชุดกระชับสัดส่วน',
        'เสื้อและกางเกงซับใน' => 'เสื้อและกางเกงซับใน',
        'กางเกงนอน' => 'กางเกงนอน',
        'เสื้อนอน' => 'เสื้อนอน',
    );
    
    foreach ($needles as $category => $need)
    {
        if ($position = stripos($content['description'], $category)) {
            $content['category'][$need] = $need;
        }
    }
}

/**
 * 
 * @param type $content
 */
function getColors(&$content, $dirname)
{
    $needles = array(
        'ดำ' => 'ดำ',
        'เทาเข้ม' => 'เทา',
        'เทาอ่อน' => 'เทา',
        'เทา' => 'เทา',
        'ขาว' => 'ขาว',
        'ครีม' => 'ครีม',
        'กรมท่า' => 'กรม',
        'กรม' => 'กรม',
        'ส้ม' => 'ส้ม',
        'ฟ้าเข้ม' => 'ฟ้า',
        'ฟ้าอ่อน' => 'ฟ้า',
        'ฟ้าทะเล' => 'ฟ้า',
        'ฟ้าน้ำทะเล' => 'ฟ้า',
        'ฟ้า' => 'ฟ้า',
        'น้ำเงินเข้ม' => 'น้ำเงิน',
        'น้ำเงินอ่อน' => 'น้ำเงิน',
        'น้ำเงิน' => 'น้ำ',
        'ม่วง' => 'ม่วง',
        'เขียวเข้ม' => 'เขียว',
        'เขียวอ่อน' => 'เขียว',
        'เขียว' => 'เขียว',
        'ชมพูเข้ม' => 'ชมพู',
        'ชมพูอ่อน' => 'ชมพู',
        'ชมพู' => 'ชมพู',
        'แดงเข้ม' => 'แดง',
        'แดงอ่อน' => 'แดง',
        'แดง' => 'แดง',
        'เหลือง' => 'เหลือง',
        'น้ำตาล' => 'น้ำตาล',
        'โอรส' => 'โอรส',
        'โอรน' => 'โอรส'
    );

    preg_match_all('/สี.*/', $content['description'], $match);

    if ( empty($match) ) {
        return;
    }

    $to_keep = array();

    foreach ($match[0] as $key => $value) {
        $to_keep[trim($value)] = trim($value);
    }

    if (!empty($dirname) && file_exists($dirname.'color.json')) {
        $colors = file_get_contents($dirname.'color.json');

        if (!empty($colors)) {
            $to_keep = array_merge(json_decode($colors, true), $to_keep);
        }
    }

    createJsonFile($dirname, $to_keep, 'color');
    
    foreach ($needles as $color => $need)
    {
        if ($position = stripos($content['description'], $color)) {
            $content['colors'][$need] = $need;
        }
    }
}

/**
 * 
 * @param type $content
 */
function getTextures(&$content, $dirname)
{
    $needles = array(
        'ผ้าชีฟอง' => 'ผ้าชีฟอง',
        'ชีฟอง' => 'ผ้าชีฟอง',
        'ชีพอง' => 'ผ้าชีฟอง',
        'ซีฟอง' => 'ผ้าชีฟอง',
        'ผ้าลูกฟูก' => 'ผ้าลูกฟูก',
        'ลูกฟูก' => 'ผ้าลูกฟูก',
        'ผ้าคอตตอน' => 'ผ้าคอตตอน',
        'คอลตอน' => 'ผ้าคอตตอน',
        'คอตตอน' => 'ผ้าคอตตอน',
        'คอตตอล' => 'ผ้าคอตตอน',
        'ผ้ายีนส์' => 'ผ้ายีนส์',
        'ยีนส์' => 'ผ้ายีนส์',
        'ผ้าฮานาโกะ' => 'ผ้าฮานาโกะ',
        'ผ้าฮานาโก๊ะ' => 'ผ้าฮานาโกะ',
        'ฮานาโกะ' => 'ผ้าฮานาโกะ',
        'ฮานาโก๊ะ' => 'ผ้าฮานาโกะ',
        'ฮานาเกะ' => 'ผ้าฮานาโกะ',
        'ฮานาเก้ะ' => 'ผ้าฮานาโกะ',
        'ฮานาโก๊ะ' => 'ผ้าฮานาโกะ',
        'ผ้ายืด' => 'ผ้ายืด',
        'ผ้าลูกไม้' => 'ผ้าลูกไม้',
        'ลูกไม้' => 'ผ้าลูกไม้',
        'ผ้าหนัง' => 'ผ้าหนัง',
        'ผ้าลินิน' => 'ผ้าลินิน',
        'ลินิน' => 'ผ้าลินิน',
        'ผ้าตาข่าย' => 'ผ้าตาข่าย',
        'ตาข่าย' => 'ผ้าตาข่าย',
        'ผ้าขนแกะ' => 'ผ้าขนแกะ',
        'ขนแกะ' => 'ผ้าขนแกะ',
        'ผ้าโพลีเอสเตอร์' => 'ผ้าโพลีเอสเตอร์',
        'โพลีเอสเตอร์' => 'ผ้าโพลีเอสเตอร์',
        'หนังซาติน' => 'หนังซาติน',
        'ซาติน' => 'หนังซาติน',
        'ผ้าไหม' => 'ผ้าไหม',
        'ผ้ากำมะหยี่' => 'ผ้ากำมะหยี่',
        'กำมะหยี่' => 'ผ้ากำมะหยี่',
        'ผ้าซาร่า' => 'อื่นๆ',
        'ผ้าซ่ารา' => 'อื่นๆ',
        'ซ่าร่า' => 'อื่นๆ',
        'ผ้าไมโคร' => 'อื่นๆ',
        'ไมโคร' => 'อื่นๆ',
        'ไมโคร' => 'อื่นๆ',
        'ไม้โคร' => 'อื่นๆ',
        'ผ้าโอซาก้า' => 'อื่นๆ',
        'ผ้าโอซาก้าโฟร์เวย์' => 'อื่นๆ',
        'ผ้าโฟร์เวย์' => 'อื่นๆ',
        'โฟร์เวย์' => 'อื่นๆ',
        'โฟว์เวย์' => 'อื่นๆ',
        'โฟเวย์' => 'อื่นๆ',
        'โฟเว' => 'อื่นๆ',
        'ผ้าโรนัลโด้' => 'อื่นๆ',
        'ผ้าโลนัลโด้' => 'อื่นๆ',
        'ผ้าโลนัลโด' => 'อื่นๆ',
        'โรนัลโด้' => 'อื่นๆ',
        'โลนัลโด้' => 'อื่นๆ',
        'ผ้าแก้ว' => 'อื่นๆ',
        'ผ้าสลาฟ' => 'อื่นๆ',
        'ผ้าซีทู' => 'อื่นๆ',
        'ผ้าสปัน' => 'อื่นๆ',
        'ผ้าสบัน' => 'อื่นๆ',
        'ผ้าหางกระรอก' => 'อื่นๆ',
        'ผ้าออแกนนิค' => 'อื่นๆ',
        'ออแกนนิค' => 'อื่นๆ',
        'ออแกรนิค' => 'อื่นๆ',
        'เรย่อน' => 'อื่นๆ',
        'เรยอน' => 'อื่นๆ',
        'ผ้าลายสก๊อต' => 'อื่นๆ',
        'ผ้าห่างกระรอก' => 'อื่นๆ',
        'ผ้าหางกระรอก' => 'อื่นๆ',
        'ผ้าแก้ว' => 'อื่นๆ',
        'ผ้าโอซาก้า' => 'อื่นๆ',
        'ผ้าไทย' => 'อื่นๆ',
        'ผ้าทอ' => 'อื่นๆ',
        'ผ้ามอสเครฟ' => 'อื่นๆ',
        'ผ้าสปิงทู' => 'อื่นๆ',
        'ผ้าป๊อปคอน' => 'อื่นๆ',
        'ผ้าแมงโก้' => 'อื่นๆ',
        'ผ้าซาติน' => 'อื่นๆ',
        'ผ้าซันเท็กซ์' => 'อื่นๆ',
    );

    preg_match_all('/ผ้า.*?\s+/i', $content['description'], $match);

    if ( empty($match) ) {
        return;
    }

    $to_keep = array();

    foreach ($match[0] as $key => $value) {
        $to_keep[trim($value)] = trim($value);
    }

    if (!empty($dirname) && file_exists($dirname.'texture.json')) {
        $textures = file_get_contents($dirname.'texture.json');

        if (!empty($textures)) {
            $to_keep = array_merge(json_decode($textures, true), $to_keep);
        }
    }

    createJsonFile($dirname, $to_keep, 'texture');

    foreach ($needles as $texture => $need)
    {
        if ($position = stripos($content['description'], $texture)) {
            $content['texture'][$need] = $need;
        }
    }
}

/**
 * 
 * @param type $content
 */
function getPatterns(&$content, $dirname)
{
    $needles = array(
        'ลายดอก' => 'ลายดอก',
        'ลายดอกไม้' => 'ลายดอก',
        'ลายลูกไม' => 'ลายดอก',
        'ลายหนังสัตว์' => 'ลายหนัง',
        'ลายหนัง' => 'ลายหนัง',
        'ลายพราง' => 'ลายพราง',
        'ลายสก้อต' => 'ลายสก็อต',
        'ลายสต็อต' => 'ลายสก็อต',
        'ลายสต๊อต' => 'ลายสก็อต',
        'ลายสก็อต' => 'ลายสก็อต',
        'ลายสกอ๊ต' => 'ลายสก็อต',
        'ลายกราฟฟิก' => 'ลายกราฟฟิกส์',
        'ลายกราฟฟิกส์' => 'ลายกราฟฟิกส์',
        'ลายกาฟฟิก' => 'ลายกราฟฟิกส์',
        'ลายกราฟฟิค' => 'ลายกราฟฟิกส์',
        'ลายตัวอักษร' => 'ลายตัวหนังสือ',
        'สีพื้น' => 'สีพื้น',
        'ลายจุด' => 'ลายจุด',
        'ลายทาง' => 'ลายทาง',
        'ลายตาราง' => 'ลายทาง',
    );

    preg_match_all('/ลาย.*?\s+/i', $content['description'], $match);

    if ( empty($match) ) {
        return;
    }

    $to_keep = array();

    foreach ($match[0] as $key => $value) {
        $to_keep[trim($value)] = trim($value);
    }

    if (!empty($dirname) && file_exists($dirname.'pattern.json')) {
        $patterns = file_get_contents($dirname.'pattern.json');

        if (!empty($patterns)) {
            $to_keep = array_merge(json_decode($patterns, true), $to_keep);
        }
    }

    createJsonFile($dirname, $to_keep, 'pattern');
    
    foreach ($needles as $pattern => $need)
    {
        if ($position = stripos($content['description'], $pattern)) {
            $content['pattern'][$need] = $need;
        }
    }
}


/**
 * 
 * @param type $content
 */
function getLengths(&$content, $dirname)
{
    $needles = array(
        'แขนสั้น' => 'แขนสั้น',
        'แขนยาว' => 'แขนยาว',
        'แขนกุด' => 'ไม่มีแขน',
        'ไม่มีแขน' => 'ไม่มีแขน',
    );
    
    foreach ($needles as $length => $need)
    {
        if ($position = stripos($content['description'], $length)) {
            $content['length'][$need] = $need;
        }
    }
}


function hashKey(&$content, $directory) 
{
    $to_hash = array();
    $content['duplicate'] = false;
    $hash = md5(md5($content['description_text']));
    $content['hash'] = $hash;
    $to_hash[$hash] = $content['id'];

    if (!empty($directory) && file_exists($directory.'hash_key.json')) {
        $hashs = file_get_contents($directory.'hash_key.json');

        if ( !empty($hashs) ) {
            $hash_list = json_decode($hashs, true);

            if ( !empty($hash_list[$hash]) && $hash_list[$hash] != $content['id'] ) {
                $content['duplicate'] = true;
                return;
            }

            $to_hash = array_merge($hash_list, $to_hash);
        }
    }

    createJsonFile($directory, $to_hash, 'hash_key');
}


/**
 * 
 * @param type $content
 * @return boolean
 */
function skipPreorder($content)
{
    preg_match('/(พรีออร์เดอร์|pre order|preorder|pre-order)/i', $content['description'], $pre_order);

    if (!empty($pre_order[1])) {
        return true;
    } else {
        return false;
    }
}


/**
 * 
 * @param type $content
 */
function clearDescriptionTag(&$content = null)
{
    $content = preg_replace('/<a.*>(.*?)<.*a>/i', '$1', $content);
    $content = preg_replace('/<img([\w\W]+?)>/i', '', $content);
}


/**
 * 
 * @param type $id
 * @param type $contents
 * @return boolean
 */
function checkValidPrice($id, &$contents)
{
    if (!empty($contents[$id]['cost_bath']) && $contents[$id]['cost_bath'] == "undefined บาท") {
        preg_match('/(ราคา?.*?|sell?.*?|sale?.*?)?([0-9]{3,5})+.*?(\..*?บาท|\.บาท|บาท|\.|bath|฿|thb)(\s)?/i', $contents[$id]['description'], $cost);

        if (!empty($cost[2])) {
            $contents[$id]['cost'] = $cost[2];
            $contents[$id]['cost_bath'] = str_replace("undefined", $cost[2], $contents[$id]['cost_bath']);

            return false;
        } else {
            return true;
        }
    }

    return false;
}


/**
 * 
 * @param type $dirname
 */
function createFolder($dirname = null)
{
    if (!is_dir($dirname)) {
        $old = umask(0);
        mkdir($dirname, 0777, true);
        umask($old);
    }
}


/**
 * 
 * @param type $dirname
 * @param type $pictures
 */
function downloadImages($dirname, $pictures)
{
    $pattern = '%s%s.jpg';

    foreach ($pictures as $id => $picture)
    {
        $file_name = sprintf($pattern, $dirname, $id);

        if (!file_exists($file_name)) {
            $node = file_get_contents($picture);
            file_put_contents($file_name, $node);
        }
    }
}


function createJsonFile($dirname, $content, $name = 'data')
{
    $pattern = '%s%s.json';

    file_put_contents(sprintf($pattern, $dirname, $name), json_encode($content, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}


function clearDetail(&$content)
{
    if (!empty($content['description'])) {
        $breaks = array("<br />", "<br>", "<br/>");
        $content['description'] = str_ireplace($breaks, "\r\n", $content['description']);
    }
}

